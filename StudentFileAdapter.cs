﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ControlWork
{
    public static class StudentFileAdapter
    {
        public static List<Student> ConvertColumnsToStudents(List<string> columns)
        {
            return columns.Select(CreateStudentFromColumn).ToList();
        }

        private static Student CreateStudentFromColumn(string line)
        {
            var columns = line.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            var lastName = columns[0];
            var firstName = columns[1];
            var patronymic = columns[2];
            var dateOfBirth = DateTime.Parse(columns[3]);
            var city = columns[4];

            return new Student(lastName, firstName, patronymic, dateOfBirth, city);
        }
    }
}