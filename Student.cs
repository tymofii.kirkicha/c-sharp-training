﻿using System;

namespace ControlWork
{
    public readonly struct Student
    {
        public readonly string LastName;
        public readonly string FirstName;
        public readonly string Patronymic;
        public readonly DateTime DateOfBirth;
        public readonly string City;

        public Student(string lastName, string firstName, string patronymic, DateTime dateOfBirth, string city)
        {
            LastName = lastName;
            FirstName = firstName;
            Patronymic = patronymic;
            DateOfBirth = dateOfBirth;
            City = city;
        }
    }
}