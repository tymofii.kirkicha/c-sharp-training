﻿using System;

namespace ControlWork
{
    internal static class Program
    {
        private static void Main()
        {
            var fileColumns = FileService.ReadStudentsFromFile("input.txt");
            var students = StudentFileAdapter.ConvertColumnsToStudents(fileColumns);
            Console.WriteLine(Environment.NewLine + "All students:");
            StudentService.PrintStudents(students);

            Console.WriteLine(Environment.NewLine + "Youngest student:");
            var youngestStudents = StudentService.FindYoungestStudent(students);
            StudentService.PrintStudent(youngestStudents);

            var averageAge = StudentService.CalculateAverageAge(students);
            Console.WriteLine(Environment.NewLine + "Average age of student: {0}", averageAge);

            const string nativeTown = "Cherkasy"; // You can change the native town
            Console.WriteLine(Environment.NewLine + "Out of town students (except {0}):", nativeTown);
            var outOfTownStudents = StudentService.FindOutOfTownStudents(students, nativeTown);
            StudentService.PrintStudents(outOfTownStudents);

            Console.ReadKey();
        }
    }
}