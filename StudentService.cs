﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ControlWork
{
    public static class StudentService
    {
        public static void PrintStudents(List<Student> students)
        {
            foreach (var student in students) PrintStudent(student);
        }

        public static void PrintStudent(Student student)
        {
            Console.WriteLine(
                "Last name: " + student.LastName + ", " +
                "First name: " + student.FirstName + ", " +
                "Patronymic: " + student.Patronymic + ", " +
                "Date of birth: " + student.DateOfBirth + ", " +
                "City: " + student.City
            );
        }

        public static Student FindYoungestStudent(List<Student> students)
        {
            var youngestStudentAge = DateTime.MinValue;

            foreach (var s in students)
            {
                var studentAge = s.DateOfBirth;

                if (DateTime.Compare(studentAge, youngestStudentAge) > 0) youngestStudentAge = studentAge;
            }

            return students.First(s => s.DateOfBirth == youngestStudentAge);
        }

        public static int CalculateAverageAge(List<Student> students)
        {
            return students
                .Select(s => CalculateAge(s.DateOfBirth))
                .Sum() / students.Count();
        }

        private static int CalculateAge(DateTime birthDate)
        {
            var yearsPassed = DateTime.Now.Year - birthDate.Year;
            if (DateTime.Now.Month < birthDate.Month ||
                DateTime.Now.Month == birthDate.Month && DateTime.Now.Day < birthDate.Day)
                yearsPassed--;

            return yearsPassed;
        }

        public static List<Student> FindOutOfTownStudents(List<Student> students, string nativeTown)
        {
            return students.Where(s => s.City != nativeTown).ToList();
        }
    }
}