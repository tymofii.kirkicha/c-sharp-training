﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ControlWork
{
    public static class FileService
    {
        public static List<string> ReadStudentsFromFile(string fileName)
        {
            ValidateFileExisting(fileName);
            var streamReader = GetStreamReader(fileName);
            var columns = ReadColumnsFromStreamReader(streamReader);
            return columns;
        }

        private static void ValidateFileExisting(string fileName)
        {
            if (IsFileExistsByFileName(fileName)) return;

            Console.WriteLine("Error: File not found by filename = " + fileName);
            throw new FileNotFoundException();
        }

        private static StreamReader GetStreamReader(string fileName)
        {
            return new StreamReader(fileName, Encoding.UTF8);
        }

        private static bool IsFileExistsByFileName(string fileName)
        {
            return File.Exists(fileName);
        }

        private static List<string> ReadColumnsFromStreamReader(TextReader textReader)
        {
            string line;
            var result = new List<string>();

            while ((line = textReader.ReadLine()) != null) result.Add(line);

            textReader.Close();
            return result;
        }
    }
}